<?php
  
  require_once('animal.php');
  require_once('ape.php');
  require_once('frog.php');
  
  $sheep = new Animal("Shaun");
  echo "Name : " . $sheep -> name . "<br>";
  echo "Legs : " . $sheep -> legs . "<br>";
  echo "Cold_Blooded : " . $sheep -> cold_blooded . "<br><br>";
 

  $kodok = new Frog("Buduk");
  echo "Name : " . $kodok -> name . "<br>";
  echo "Legs : " . $kodok -> legs . "<br>";
  echo "Cold_Blooded : " . $kodok -> cold_blooded . "<br>";
  $kodok->Jump("Hop Hop <br><br>");

  $sungokong = new Ape("Kera Sakti");
  echo "Name : " . $sungokong -> name . "<br>";
  echo "Legs : " . $sungokong -> legs . "<br>";
  echo "Cold_Blooded : " . $sungokong -> cold_blooded . "<br>";
  $sungokong->Yell("Auooooo <br><br>") 
  


?>